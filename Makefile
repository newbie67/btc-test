DEFAULT_GOAL := up

UID := $(shell id -u)
GID := $(shell id -g)
export UID
export GID

up:
	docker-compose up

update:
	docker-compose run --rm app composer update
	docker-compose run --rm app chown -R ${UID}:${GID} vendor

tests:
	docker-compose run --rm app vendor/bin/phpunit
