<?php

declare(strict_types=1);

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

$stdoutLoggerHandler = new ErrorLogHandler();
$stdoutLoggerHandler->setFormatter(
    new LineFormatter(">>> %level_name% | %datetime% : %message% %context% %extra%\n", 'Y.m.d H:i:s')
);

$fileHandler = new StreamHandler(dirname(__DIR__) . '/tmp/logs/app.error.log', Logger::ERROR);

$config = [
    'doctrine' => [
        'dev_mode' => true,
        'cache_dir' => dirname(__DIR__) . '/tmp/doctrine',
        'metadata_dirs' => [
            dirname(__DIR__) . '/src/Entity',
        ],
        'connection' => [
            'driver' => 'sqlite:////usr/local/var/db.sqlite',
            'url' => 'sqlite:////root/db/database.sqlite',
        ],
    ],
    'db' => [
        'driver' => 'pdo_mysql',
        'user' => 'root',
        'password' => '',
        'dbname' => 'foo',
    ],
    'logger' => new Logger('app', [
        $stdoutLoggerHandler,
        $fileHandler,
    ]),
    'is_production' => getenv('ENV') === 'prod',
];

if (file_exists(__DIR__ . '/local.config.php')) {
    $configOverlay = require __DIR__ . '/local.config.php';
    $config = array_replace_recursive($config, $configOverlay);
}

return $config;
