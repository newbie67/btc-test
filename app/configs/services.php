<?php

declare(strict_types=1);

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use Psr\Log\LoggerInterface;

//use Doctrine\Common\Cache\FilesystemCache;
//use Doctrine\Common\Cache\Psr6\CacheAdapter;

$config = require __DIR__ . '/config.php';

return [
    'config' => function () use ($config): array {
        return $config;
    },
    LoggerInterface::class => function () use ($config): LoggerInterface {
        return $config['logger'];
    },
    EntityManagerInterface::class => function () use ($config): EntityManagerInterface {
        $emConfig = Setup::createAnnotationMetadataConfiguration(
            $config['doctrine']['metadata_dirs'],
            $config['doctrine']['dev_mode']
        );
        $emConfig->setMetadataDriverImpl(
            new AnnotationDriver(new AnnotationReader, $config['doctrine']['metadata_dirs'])
        );
//        $emConfig->setMetadataCache(CacheAdapter::wrap(new FilesystemCache($config['doctrine']['cache_dir'])));

        return EntityManager::create($config['doctrine']['connection'], $emConfig);
    }
];
