<?php

declare(strict_types=1);

namespace App;

use App\Component\AnnotatedRouteControllerLoader;
use App\Entity\CalculatorResult;
use DI\ContainerBuilder;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Exception;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\NoConfigurationException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Loader\AnnotationDirectoryLoader;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;

class Application
{
    private FileLocatorInterface $configLocator;
    private ContainerInterface $container;
    private LoggerInterface $logger;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->configLocator = new FileLocator(dirname(__DIR__) . '/configs');

        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions($this->configLocator->locate('services.php'));
        $this->container = $containerBuilder->build();

        $this->logger = $this->container->get(LoggerInterface::class);
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function run(): void
    {
        $annotationRouteLoader = new AnnotationDirectoryLoader(
            $this->configLocator,
            new AnnotatedRouteControllerLoader(new AnnotationReader()),
        );

        $router = new Router(
            $annotationRouteLoader,
            __DIR__ . '/Controller/',
            [
                'cache_dir' => null, // todo
                'debug' => true, // todo
            ],
            new RequestContext('/'),
            $this->logger,
        );
        $this->executeRoute($router);
    }

    private function executeRoute(RequestMatcherInterface $routeMatcher): void
    {
        $request = Request::createFromGlobals();
        try {
            $parameters = $routeMatcher->matchRequest($request);
            $parts = explode('::', $parameters['_controller'], 2);

            $controller = $this->container->get($parts[0]);
            $action = $parts[1] ?? null;

            /** @var Response $response */
            $response = null === $action ? $controller($request) : $controller->{$action}($request);
            if (!$response instanceof Response) {
                throw new Exception('Actions must return instance ' . Response::class);
            }
            $response->send();
        } catch (NoConfigurationException | ResourceNotFoundException $e) {
            $this->logger->info($e->getMessage(), ['trace' => $e->getTrace()]);
            $this->pageNotFound($e->getMessage());
        } catch (MethodNotAllowedException $e) {
            $this->logger->info($e->getMessage(), ['trace' => $e->getTrace()]);
            $this->methodNotAllowed();
        }
    }

    private function pageNotFound(string $message): void
    {
        $response = new Response($message, 404);
        $response->send();
    }

    private function methodNotAllowed(): void
    {
        $response = new Response('Method not allowed', 403);
        $response->send();
    }
}
