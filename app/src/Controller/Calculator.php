<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\Calculator\DivisionByZeroException;
use App\Exception\Calculator\InvalidInputException;
use App\Service\Calculator as CalculatorService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Calculator
{
    private CalculatorService $calculator;

    public function __construct(CalculatorService $calculator)
    {
        $this->calculator = $calculator;
    }

    /**
     * @Route("/calculate", requirements={"input"="[a-z]"})
     */
    public function calculate(Request $request): Response
    {
        if (!empty($request->get('input'))) {
            try {
                return $this->response(['result' =>  $this->calculator->calculate($request->get('input'))]);
            } catch (InvalidInputException | DivisionByZeroException $e) {
                return $this->response([
                    'message' => 'calculator error: ' . $e->getMessage(),
                ], false, Response::HTTP_BAD_REQUEST);
            }
        } else {
            return $this->response([
                'message' => 'required parameter: input',
            ], false, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/last-results")
     */
    public function getLastResults(Request $request): Response
    {
        return $this->response([
            'calculate_results' => $this->calculator->getLastResults(),
        ]);
    }

    private function response(array $data, bool $success = true, int $code = Response::HTTP_OK): JsonResponse
    {
        $data['success'] = $success;
        return (new JsonResponse($data, $code))->setEncodingOptions(JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }
}
