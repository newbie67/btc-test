<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable;

/**
 * @ORM\Entity
 * @ORM\Table(name="calculator_result")
 */
class CalculatorResult implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $input;

    /**
     * @ORM\Column(type="decimal")
     */
    private float $result;

    /**
     * @ORM\Column(type="integer", name="created_at")
     */
    private int $createdAt;

    public function __construct(string $input, float $result)
    {
        $this->input = $input;
        $this->result = $result;
        $this->createdAt = time();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getInput(): string
    {
        return $this->input;
    }

    public function getResult(): float
    {
        return $this->result;
    }

    public function getCreatedAt(): DateTime
    {
        return (new DateTime())->setTimestamp($this->createdAt);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'input' => $this->getInput(),
            'result' => $this->getResult(),
            'created_at' => $this->getCreatedAt()->getTimestamp(),
        ];
    }
}
