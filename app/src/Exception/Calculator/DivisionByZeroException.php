<?php

declare(strict_types=1);

namespace App\Exception\Calculator;

use Exception;

class DivisionByZeroException extends Exception
{

}
