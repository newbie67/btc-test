<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\CalculatorResult;
use App\Exception\Calculator\DivisionByZeroException;
use App\Exception\Calculator\InvalidInputException;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class Calculator
{
    private const LAST_RESULT_COUNT = 5;

    private LoggerInterface $logger;
    private EntityManagerInterface $entityManager;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->entityManager = $em;
    }

    /**
     * @return CalculatorResult[]
     */
    public function getLastResults(): array
    {
        return $this->entityManager->getRepository(CalculatorResult::class)->findBy(
            [],
            ['createdAt' => Criteria::DESC],
            self::LAST_RESULT_COUNT,
        );
    }

    /**
     * @throws DivisionByZeroException
     * @throws InvalidInputException
     */
    public function calculate(string $str): float
    {
        $input = $str;

        $this->logger->debug('calculator input string: ' . $str);
        $str = $this->normalizeInputMathSymbols($str);

        $matches = [];
        preg_match('#[\+|\-|\/|\*]{2,}#', $str, $matches);
        if (count($matches) > 0) {
            throw new InvalidInputException('invalid group characters: ' . implode(', ', $matches));
        }

        $unavailableSymbols = preg_replace('#([0-9]|\/|\*|\-|\+|\(|\)|\.)#', '', $str);
        if ($unavailableSymbols != '') {
            throw new InvalidInputException('input characters: ' . $unavailableSymbols);
        }

        $res = $this->calculateExpression($str);

        $this->logger->debug('calculator result: ' . $str);

        $this->entityManager->persist(new CalculatorResult($input, $res));
        $this->entityManager->flush();

        return $res;
    }

    /**
     * @throws DivisionByZeroException
     * @throws InvalidInputException
     */
    private function calculateExpression(string $str): float
    {
        do {
            $closedBracketPos = stripos($str, ')');
            if (false !== $closedBracketPos) {
                $nearestOpenBracketPos = strripos(substr($str, 0, $closedBracketPos), '(');
                if (false === $nearestOpenBracketPos) {
                    throw new InvalidInputException('opened bracket have to has closed bracket');
                }
                $exprInBrackets = substr(
                    $str,
                    $nearestOpenBracketPos + 1,
                    $closedBracketPos - $nearestOpenBracketPos - 1,
                );
                $exprValue = $this->calculateMathExpression($exprInBrackets);
                $str = substr($str, 0, $nearestOpenBracketPos) . $exprValue . substr($str, $closedBracketPos + 1);
            }
        } while ($closedBracketPos != false);

        $openBracketPos = strripos($str, '(');
        if (false !== $openBracketPos) {
            throw new InvalidInputException('closed bracket have to has opened bracket');
        }

        return $this->calculateMathExpression($str);
    }

    private function normalizeInputMathSymbols(string $str): string
    {
        $str = trim($str, '=');
        $str = preg_replace('#\s+#', '', $str);
        return ltrim($str, '+');
    }

    /**
     * @param string $str math expression without brackets
     *
     * @return float
     *
     * @throws DivisionByZeroException
     */
    private function calculateMathExpression(string $str): float
    {
        $plus = stripos($str, '+');
        if (false !== $plus) {
            $str = $this->calculateMathExpression(substr($str, 0, $plus))
                + $this->calculateMathExpression(substr($str, $plus + 1));
        }

        $minus = strripos((string)$str, '-');
        if (false !== $minus) {
            $str = $this->calculateMathExpression(substr($str, 0, $minus))
                - $this->calculateMathExpression(substr($str, $minus + 1));
        }

        $multiplication = stripos((string)$str, '*');
        if (false !== $multiplication) {
            $str = $this->calculateMathExpression(substr($str, 0, $multiplication))
                * $this->calculateMathExpression(substr($str, $multiplication + 1));
        }

        $division = stripos((string)$str, '/');
        if (false !== $division) {
            $divisionTo = $this->calculateMathExpression(substr($str, $division + 1));
            if ($divisionTo == 0) {
                throw new DivisionByZeroException();
            }
            $str = $this->calculateMathExpression(substr($str, 0, $division)) / $divisionTo;
        }

        return (float)$str;
    }
}
