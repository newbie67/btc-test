<?php

declare(strict_types=1);

namespace Tests;

use App\Application;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class BaseTestCase extends TestCase
{
    protected ?ContainerInterface $container;

    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        $app = new Application();
        $this->container = $app->getContainer();

        parent::setUp();
    }
}
