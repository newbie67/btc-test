<?php

declare(strict_types=1);

namespace Tests\Controller;

use App\Controller\Calculator;
use Symfony\Component\HttpFoundation\Request;
use Tests\BaseTestCase;

class CalculatorTest extends BaseTestCase
{
    public function testCalculate()
    {
        $controller = $this->getController();
        $request = new Request(['input' => '2+2']);

        $response = $controller->calculate($request)->getContent();
        $response = json_decode($response);

        $this->assertEquals(true, $response->success);
        $this->assertEquals(4, $response->result);
    }

    public function testGetLastResults()
    {
        $controller = $this->getController();

        $controller->calculate(new Request(['input' => '1']));
        $controller->calculate(new Request(['input' => '2']));
        $controller->calculate(new Request(['input' => '3']));
        $controller->calculate(new Request(['input' => '4']));
        $controller->calculate(new Request(['input' => '5']));
        $controller->calculate(new Request(['input' => '6']));


        $response = $controller->getLastResults(new Request())->getContent();
        $response = json_decode($response);

        $this->assertCount(5, $response->calculate_results);

        $this->assertEquals(6, $response->calculate_results[0]->result);
        $this->assertEquals(5, $response->calculate_results[1]->result);
        $this->assertEquals(4, $response->calculate_results[2]->result);
        $this->assertEquals(3, $response->calculate_results[3]->result);
        $this->assertEquals(2, $response->calculate_results[4]->result);
    }

    private function getController(): Calculator
    {
        return $this->container->get(Calculator::class);
    }
}
