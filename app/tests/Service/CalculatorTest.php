<?php

declare(strict_types=1);

namespace Tests\Service;

use App\Exception\Calculator\DivisionByZeroException;
use App\Exception\Calculator\InvalidInputException;
use App\Service\Calculator;
use Tests\BaseTestCase;

class CalculatorTest extends BaseTestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testCalculateSuccess(string $input, float $expected)
    {
        /** @var Calculator $calculator */
        $calculator = $this->container->get(Calculator::class);

        $this->assertSame($calculator->calculate($input), $expected);
    }

    /**
     * @dataProvider invalidInputDataProvider
     */
    public function testCalculateInvalidInput(string $input)
    {
        /** @var Calculator $calculator */
        $calculator = $this->container->get(Calculator::class);

        $this->expectException(InvalidInputException::class);
        $calculator->calculate($input);
    }

    public function testCalculateDivisionByZero()
    {
        /** @var Calculator $calculator */
        $calculator = $this->container->get(Calculator::class);

        $this->expectException(DivisionByZeroException::class);
        $calculator->calculate('9+6-1*(4/(14-1-13)+1)');
    }

    public function dataProvider(): array
    {
        return [
            ['1+1', 2],
            ['(1+1)', 2],
            ['2*(2+2)', 8],
            ['+2*(2+2)', 8],
            ['2*(2+4*(2+2))', 36],
            ['-1', -1],
            ['-10*(24+4)', -280],
            ['(5-(6-0))*10', -10],
            ['1.2+1.2', 2.4],
        ];
    }

    public function invalidInputDataProvider(): array
    {
        return [
            ['1++1'],
            ['1--2'],
        ];
    }
}
